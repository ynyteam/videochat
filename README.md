# videoChat

app pour poser des question en direct via la live tv Vinci

## dev

pour les scripts ``` npm run script:dev ``` lance la compilation des scripts
pour les scripts ``` npm run server ``` lance le serveur socket.io
pour les scripts ``` npm run server:dev ``` lance le serveur socket.io en mode 'dev' (debug + nodemon)
pour les scripts ``` npm run css ``` compile les styles
pour les scripts ``` npm run css:dev ``` watch les styles

c'est une mini spa qui parle a un serveur socket.io, la doc viendra quand cela sera plus qu'un poc
