const static = require('node-static')
const file = new static.Server('./public');
const users = {}
const util = require('util')
const debug = require('debug')('videoChat:app')
const port = 8080

const os = require('os');

function getLocalIp () {
	const ifaces = os.networkInterfaces();
	const ip = []
	Object.keys(ifaces).forEach(function (ifname) {
		var alias = 0;

		ifaces[ifname].forEach(function (iface) {
			if ('IPv4' !== iface.family || iface.internal !== false) {
				// skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
				return;
			}

			if (alias >= 1) {
				// this single interface has multiple ipv4 addresses
				debug(ifname + ':' + alias, iface.address);
				ip.push(iface.address)
			} else {
				// this interface has only one ipv4 adress
				debug(ifname, iface.address);
				ip.push(iface.address)
			}
			++alias;
		});
	});
	return ip[0]
}

function handler (request, response) {
		request.addListener('end', function () {
        file.serve(request, response);
    }).resume()
}


// http server serving the html files and the socket.io script
const app = require('http').createServer(handler).listen(port, function () {
	console.log('app started: visit http://'+ getLocalIp() + ':' + port)
})


// socket.io connextion to the server
const io = require('socket.io').listen(app)

function updateStatus (clientId, freq, socket) {
	return setInterval(function () {

	})
}

// socket.io events
// connection
io.on('connection', function(socket){
  debug('a user connected', socket.id);
	users[socket.id]= {clientType:'',isBusy:false, isReady : false}
	// chat message
	socket.on('client:mgs', function(msg){
    debug('message: ' + msg)
  })
	// chat message
	socket.on('clientType', function(msg){
		users[socket.id].clientType = msg
    debug('users: ' + util.inspect(users))
  })

	socket.on('user:name', function(msg){
		users[socket.id].userName = msg
		users[socket.id].isReady = true
    debug('users: ' + util.inspect(users))
  })

	socket.on('chat message', function(msg){
		socket.broadcast.emit('chat message', msg);
  });

  socket.on('disconnect', function(){
    debug('user disconnected', socket.id);
		delete users[socket.id]
		debug(users)
  });

})

