const bel = require('bel')
const debug = require('debug')('video:chatWindow')
/**
 * create a chat window, yay
 *
 * @param {Array} an array of messages
 * @param {Function} onsubmit action when submitting a message
 * @returns
 */
module.exports = function (msgs, onsubmit) {
	let curMsg = ''
	function render () {
		cMsg=''
		debug('render called msg', cMsg, 'ropt')
    return bel`
		<div class="chatCont flexCont">
			<div class="chatWindow">
				<div class="textCont" id="messages">
					${msgs.map(function (msg) {
						return bel`<div class="msg"><span>${msg}</span></div>`
					})}
				</div>
			</div>
			<div class="wrtBar">
					<input id="m" autocomplete="off" placeholder='type your message here' value =${cMsg} onkeypress=${updateCurMsg} />
					${button('subBt','send')}
			</div>
		</div>
		`
  }
	function updateCurMsg (e) {
		debug(curMsg)
		curMsg = e.srcElement.value
	}
	function button (id, label) {
    return bel`<button id= 'submitBt' onclick=${function (e) {
      // Then action gets sent up
			debug('sendBt clicked', cMsg)
      onsubmit(e, cMsg)
			cMsg = ''
			render()
    }}>${label}</button>`
  }
	var element = render()
	return element
}
