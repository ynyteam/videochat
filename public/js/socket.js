var debug = require('debug')('video:sockets')

function openSocket (io) {
	debug('coolos', typeof io)
	debug(io)
	const socket = io()
	return socket
}

function SendMsg (socket) {
	return function (msg) {
		socket.emit('client:mgs', msg)
	}
}

function createSocket (io) {

	const sock = {}
	sock.socket = openSocket(io)
	sock.sendMsg = SendMsg(sock.socket)
	return sock
}

module.exports = createSocket
