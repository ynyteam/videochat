/* global io, $, console */
const debug = require('debug')('video:client')
var bel = require('bel')
// console.log('yo')
const MakeSocket = require('./socket')
const ChatWindow = require('../components/chatWindow')
const socket = MakeSocket(io)
debug(socket)

const defaultConf = {
	msgs: ['welcome to vinci Chat, yo!']
}

function sendMsg (msg) {
	debug(msg)
}

function onsubmit (e, msg) {
	e.preventDefault()
	if (msg === '') return false
	debug(msg)
}

function createChat (tarEl, msgs = defaultConf) {
	const elem = ChatWindow(['jerome: bonsoir gael', 'gael: bonsoir!'], onsubmit)
	document.getElementById(tarEl).appendChild(elem)
}


function getClientInfos () {
	const clientInfos = {}
	clientInfos.id = document.getElementById('learner_id').innerHTML
	const fullName = document.getElementById('learner_name').innerHTML.split(',')

	clientInfos.firstName = fullName[1]
	clientInfos.LastName = fullName[0]
	return clientInfos
}

function init () {
	cInfos = getClientInfos()
	debug(cInfos)
	const socket = MakeSocket(io)
	socket.sendMsg('yo')
	createChat('wrapEl')
}



init()
